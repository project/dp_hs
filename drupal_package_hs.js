(function ($) {
	$(document).ready(function() {
        window.ajax_views_form_drupal_package_hs = function(obj, ajax_url){
            var ajax = new Drupal.ajax(false, obj, {
                url: ajax_url,
            });
            ajax.eventResponse(ajax, {});
            return false;
        }
	});
})(jQuery);