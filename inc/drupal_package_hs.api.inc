<?php

function drupal_package_hs_callback($name = FALSE, $current_display = FALSE){
	$commands = array();		
	if(!empty($name) && !empty($current_display)){
		$_GET = $_POST + $_GET;
		$view = views_get_view($name);
	  	$view->set_display($current_display);
	  	$view->init_handlers();
	  	$exposed_form = $view->display_handler->get_plugin('exposed_form');
		$commands[] = ajax_command_replace('form[id^="views-exposed-form-'.str_replace('_', '-', $name).'-'.str_replace('_', '-', $current_display).'"]', $exposed_form->render_exposed_form(TRUE));
		if(!empty($_SESSION['messages'])){
			unset($_SESSION['messages']);
		}
		$commands[] = ajax_command_invoke('form[id^="views-exposed-form-'.str_replace('_', '-', $name).'-'.str_replace('_', '-', $current_display).'"] .error', 'removeClass', array('error'));
	}
	return array('#type' => 'ajax', '#commands' => $commands);
}